# Création d'une simple To Do List en HTML, CSS et Javascript : Pas à Pas

## Qu'est ce qu'une To Do List ?

Pour définir notre To Do List (_liste de tâches_) on a rédigé un petit _cahier des charges_ explicitant quelles sont les fonctionnalités nécessaires à la mise en place de notre application.

### Cahier des Charges de notre `To Do List` :

Une _To Do List_ est une _Liste de Tâches_ permettant de consigner des intitulés de tâches à compléter.

#### Fonctionnalités

1. Ajouter à l'aide d'un champ texte un nouvel intitulé de tâche à notre liste
2. Supprimer une tâche de notre liste
3. Marquer une tâche de notre liste comme "complétée", la séparant du reste des tâches "à compléter"

#### Fonctionnalités secondaires

1. Possibilité de modifier l'intitulé d'une tâche
2. Possibilité d'indiquer une date buttoir (_deadline_) pour une tâche

Une fois la définition de notre application est sur papier (certes sommairement), il ne nous reste plus qu'à...

## Commencer à coder

### Le Squelette de notre document

Notre page commence comme beaucoup d'autres :

```HTML
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>To Do List</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
</head>
<body>

    <script src='main.js'></script>
</body>
</html>
```

On place le script `main.js` à la fin de notre `body`, avant sa fermeture, pour être sûr que le script exécuté le sera après avoir chargé _tout_ le document.

### Mise en place de notre Champ Texte

Pour pouvoir récupérer des données textuelles envoyées par un utilisateur, il faut rendre disponible à cet utilisateur un _champ texte_ dans lequel écrire.

En HTML, de nombreux [champs](https://developer.mozilla.org/fr/docs/Learn/Forms/Basic_native_form_controls) sont disponibles dans le cadre de la [création de formulaires](https://developer.mozilla.org/fr/docs/Learn/Forms/Your_first_form). Ces champs permettent d'indiquer la possibilité de renseigner certaines informations, et un champ _textuel_ permet à l'utilisateur de librement renseigner un texte.

Pour insérer un champ texte dans notre page, on utilise l'élément `<input type="text>`, l'attribut `type` permettant de définir le format de donnée attendu.

```html
<body>
    <input type="text" />

    <script src="main.js"></script>
</body>
```

Cet élément input se manifeste sur notre page par un champ dans lequel il est possible d'écrire librement, mais qui ne fait encore rien de particulier :

![capture du champ input](screen1.png)

### Accéder et manipuler la valeur du champ

Pour pouvoir utiliser cette valeur, afin de pouvoir nommer nos tâches à partir de ce champ, il faut qu'on puisse la lire en _javascript_.
Pour récupérer facilement l'information, on peut attribuer un `id` à notre élément `<input>`, de façon à pouvoir utiliser `getElementById()` dans notre javascript.

Dans notre fichier `html`:

```html
<body>
    <!--on donne un id "taskInput" au champ -->
    <input type="text" id="taskInput" />

    <script src="main.js"></script>
</body>
```

Dans notre fichier `js` :

```js
//on récupère la référence du DOM de notre champ input
const taskInput = document.getElementById("taskInput");
```

#### Holà holà, qu'est ce qu'on vient de faire là au juste ?

Dans notre script, qui s'exécute au lancement de la page, on souhaite pouvoir _manipuler_ les éléments de notre interface. Pour pouvoir les manipuler, il faut pouvoir exécuter du _code_ qui _concerne_ les éléments qui nous intéressent.

Comme pour toute chose en programmation, si on souhaite la manipuler, il faut un moyen de l'_appeler_.

Cependant, on ne peut pas juste se contenter de dire `monchampInput` ou quelque chose du genre. Il faut préciser à notre script _quel élément_ de notre DOM (le document) on souhaite appeler. Pour ça, on peut indiquer une référence unique (l'`id` précisé dans notre HTML) de notre élément, qui permettra ensuite de le _stocker dans une variable_.

La ligne de code qu'on vient d'écrire permet donc de :

1. récupérer l'élément du DOM dont l'`id` est `"taskInput"`
2. stocker cet élément dans une _constante_ nommée `taskInput`

Rien de plus, rien de moins !

Maintenant que notre élément est accessible au travers de notre constante, on peut....

#### Accéder à la valeur du champ programmatiquement

Pour l'instant, écrire dans notre champ n'a aucun impact sur quoi que ce soit. Mais maintenant qu'on peut accéder à l'élément et à ses _propriétés_, on peut lire la _valeur_ du texte écrit dans notre champ.

Pour ce faire, on utilise la propriété `value` de notre élément :

`main.js` :

```js
//on récupère la référence du DOM de notre champ input
const taskInput = document.getElementById("taskInput");
//on lit la valeur de notre élément
console.log(taskInput.value);
```

`taskInput.value` permet d'aller chercher la _valeur_ de la _propriété_ `value` stockée dans `taskInput`, laquelle représente le texte écrit _dans le champ_ sur notre page.

Maintenant, qu'est ce qu'on trouve dans notre console ?

![valeur de notre champ input dans la console](screen2.png)

#### Génial, une chaîne vide !

Effectivement. On nous renvoie une chaîne vide. Mais c'est normal ! Au moment du chargement du script, notre champ de formulaire est effectivement vide.

Il ne faut donc pas se contenter de récupérer la valeur au _chargement de la page_, mais au moment opportun, lorsque l'utilisateur le _décide_.
On peut donc se débarrasser de notre `console.log` pour l'instant.

### Bouton et Clic de Bouton

Pour pouvoir savoir quand l'utilisateur souhaite ajouter sa tâche, et donc _quand lire_ notre champ, il faut qu'on lui donne l'opportunité de le signaler dans l'interface.
On peut le faire au travers d'un bouton :

#### Création d'un Bouton

Dans notre fichier `HTML` :

```html
<body>
    <input type="text" id="taskInput" />
    <!-- Ajout d'un bouton intitulé "Add", avec l'id "addBtn" -->
    <button id="addBtn">Add</button>
    <script src="main2.js"></script>
</body>
```

Ce qui donne à l'écran :

![Ajout de notre bouton dans la page](screen3.png)

Puis dans notre fichier `js` :

```js
//on récupère la référence du DOM de notre champ input
const taskInput = document.getElementById("taskInput");
// on récupère également la réf de notre bouton Add
const addBtn = document.getElementById("addBtn");
```

On récupère la ref de notre bouton pour pouvoir réagir aux clics effectués dessus.

#### Réagir au clic de Bouton

Pour réagir à un clic effectué sur le bouton, il faut ajouter un `EventListener` (ou _Gestionnaire d'Evènements_) à notre bouton.
Pour ce faire on utilise `addEventListener` :

```js
//on récupère la référence du DOM de notre champ input
const taskInput = document.getElementById("taskInput");
// on récupère également la réf de notre bouton Add
const addBtn = document.getElementById("addBtn");
//on ajoute un gestionnaire d'évènements sur notre bouton pour surveiller les clics de souris
addBtn.addEventListener("click");
```

Ici, on ajoute un _gestionnaire d'évènements_ sur notre bouton réagissant aux clics. On lui indique désirer réagir aux évènements de clic en précisant la chaîne `"click"`. Cette chaîne n'est _pas là par hasard_, elle correspond à une clé unique correspondant à ce type d'évènement. On peut retrouver la [liste des évènements et leurs clés sur la MDN](https://developer.mozilla.org/fr/docs/Web/Events).

Notre gestionnaire d'évènements sait désormais à quoi réagir, mais par contre, on ne lui dit pas _quoi_ faire ! Il faut donc rajouter une _fonction_ comme paramètre de notre `EventListener` pour lui indiquer _quoi faire_ en cas de clic.

```js
//on récupère la référence du DOM de notre champ input
const taskInput = document.getElementById("taskInput");
// on récupère également la réf de notre bouton Add
const addBtn = document.getElementById("addBtn");
//on ajoute un gestionnaire d'évènements sur notre bouton pour surveiller les clics de souris
addBtn.addEventListener("click", () => {
    console.log("coucou");
});
```

Maintenant, lorsque l'on clique sur notre bouton, la console nous affiche :

![Affichage de coucou au clic du bouton](screen4.png)

Superbe !

Il ne nous reste plus qu'à remplacer `"coucou"` par une valeur plus intéressante, comme `taskInput.value` par exemple :

```js
//on récupère la référence du DOM de notre champ input
const taskInput = document.getElementById("taskInput");
// on récupère également la réf de notre bouton Add
const addBtn = document.getElementById("addBtn");
//on ajoute un gestionnaire d'évènements sur notre bouton pour surveiller les clics de souris
addBtn.addEventListener("click", () => {
    console.log(taskInput.value); // <---
});
```

Et en cliquant sur notre bouton, on voit se copier la valeur de notre champ dans notre console :

![Valeur du champ loggée dans la console](screen5.png)

### Ajout d'une nouvelle tâche

Notre valeur de champ est désormais récupérée et affichée dans la console au moment du clic de bouton. Mais on doit encore régler un important problème : il faut que notre tâche apparaisse à l'écran, et non dans la console, et qu'elle y reste !

#### Créer une liste

La logique voudrait que l'on utilise une liste pour afficher nos éléments dans notre `To Do List`, et nous ne feront pas défaut à la logique. Cependant, on ne peut pas écrire les éléments de la liste en avance dans notre HTML, il faut que les éléments soient créés _à la volée_, et insérés dans notre liste !

On ajoute donc une liste vide, avec un `id` pour pouvoir la manipuler facilement, dans notre fichier HTML :

```html
<body>
    <input type="text" id="taskInput" />
    <button id="addBtn">Add</button>
    <ul id="tasklist"></ul>
    <script src="main2.js"></script>
</body>
```

Rien ne s'affiche encore à l'écran, car la liste est vide.

On doit ensuite pouvoir manipuler la liste, il faut donc pouvoir l'_appeler_ :

```js
//on récupère la référence du DOM de notre champ input
const taskInput = document.getElementById("taskInput");
// on récupère également la réf de notre bouton Add
const addBtn = document.getElementById("addBtn");
// et la ref de notre liste de tâches
const taskList = document.getElementById("taskList"); // <---

//on ajoute un gestionnaire d'évènements sur notre bouton pour surveiller les clics de souris
addBtn.addEventListener("click", () => {
    console.log(taskInput.value);
});
```

Mais que faire avec cette `TaskList` ?

#### Créer un élément

Notre `taskList` ne servira que de _receptacle_ à nos futurs éléments de liste `<li>` que l'on devra créer à la volée.

On va donc se charger de créer ces éléments dans un premier temps.

```js
addBtn.addEventListener("click", () => {
    //création d'un élément li
    const newTask = document.createElement("li"); // <----
});
```

`document.createElement()` est une méthode du `Document` permettant la création d'un nouvel élément. On précise `"li"` en paramètre pour indiquer qu'on souhaite créer un nouvel élément `<li>`.

A chaque clic, désormais, un élément `<li>` sera créé et stocké dans une variable `newTask`.

Mais lors de l'exécution de notre code, rien ne se passe !

#### Insérer un élément

Il nous faut désormais insérer l'élément dans notre `taskLst`:

```js
addBtn.addEventListener("click", () => {
    //création d'un élément li
    const newTask = document.createElement("li");
    //rangement de l'élément li créé dans notre liste
    taskList.appendChild(newTask); // <----
});
```

`parent.appendChild(enfant)` permet d'ajouter un élément `enfant` à l'_intérieur_ d'un élément `parent`. On demande donc à insérer notre élément nouvellement créé `newTask` dans l'élément déjà présent dans le document `TaskList` ajoutant notre `<li>` dans notre `<ul>`.

En appuyant sur le bouton, on obtient ce résultat :

![ajout de notre li au ul](screen6.png)

Un `<li>` s'est bien ajouté à notre liste `<ul>`, mais il est vide !

#### Renseigner le contenu d'un élément

On doit donc désormais renseigner son _contenu textuel_ en changeant une de ses propriétés, `textContent`. La valeur qu'on souhaite écrire dans notre `<li>` étant celle de notre `taskInput`, on va donc utiliser `taskInput.value` pour renseigner le contenu de notre élément de liste :

```js
addBtn.addEventListener("click", () => {
    //création d'un élément li
    const newTask = document.createElement("li");

    //changement du texte de notre tâche
    newTask.textContent = taskInput.value; // <----

    //rangement de l'élément li créé dans notre liste
    taskList.appendChild(newTask);
});
```

Et le résultat est stupéfiant :

![Ajout de notre libellé de tâche](screen7.png)

Autant de clics rajouteront autant de tâches, celles-ci représentées par des `<li>` !

#### En résumé :

Au moment du clic sur notre bouton _Add_ on effectue les actions suivantes :

1. Création d'un élément `<li>`
2. Changement du texte de l'élément `<li>` en la valeur du champ `<input>`
3. Insertion de l'élément `<li>` dans un parent, le `<ul id="taskList">`

Magnifique ! Mais attelons nous désormais à la...

### Suppression d'une tâche

Pour pouvoir supprimer une tâche, il faut donner l'opportunité à l'utilisateur de le faire. Il faut donc lui proposer un composant d'interface dédié à la suppression d'une tâche.

Un bouton par exemple.

Mais cette fois-ci, rajouter un bouton ne pourra pas se faire au travers de notre fichier `html`, puisqu'on voudrait qu'il soit attaché à nos tâches; qui sont elles créées à la volée.

Qu'à cela ne tienne, on sait créer des éléments maintenant, alors créons un `<button>` et insérons le dans le `<li>` :

```js
addBtn.addEventListener("click", () => {
    //création d'un élément li
    const newTask = document.createElement("li");
    //changement du texte de notre tâche
    newTask.textContent = taskInput.value;
    //rangement de l'élément li créé dans notre liste
    taskList.appendChild(newTask);

    //création d'un bouton de suppression
    const deleteBtn = document.createElement("button"); // <----
    //définition du texte du bouton
    deleteBtn.textContent = "Delete"; // <----
    //ajout du bouton à notre tâche
    newTask.appendChild(deleteBtn); // <----
});
```

On suit le même mode opératoire qu'avec nos `<li>`, c'est à dire qu'on crée un `<button>`, auquel on assigne un contenu textuel, qu'on insère ensuite dans un parent (qui ici se trouve être notre `<li>` créé juste avant).

Le résultat est stupéfiant :

![ajout d'un bouton delete sur chaque li](screen8.png)

Mais ce bouton ne fait rien. Il doit pourtant supprimer la tâche, mais on ne lui a encore rien dit de faire !

#### Réagir au clic de Bouton, 2ème partie

On va donc ajouter un autre `EventListener`, cette fois sur notre `deleteBtn` nouvellement créé !

```js
addBtn.addEventListener("click", () => {
    //création d'un élément li
    const newTask = document.createElement("li");
    //changement du texte de notre tâche
    newTask.textContent = taskInput.value;
    //rangement de l'élément li créé dans notre liste
    taskList.appendChild(newTask);

    //création d'un bouton de suppression
    const deleteBtn = document.createElement("button");
    //définition du texte du bouton
    deleteBtn.textContent = "Delete";
    //ajout du bouton à notre tâche
    newTask.appendChild(deleteBtn);

    //au clic sur le bouton delete
    deleteBtn.addEventListener("click", () => {
        //on retire la tâche
        newTask.remove();
    });
});
```

#### C'est quoi `newTask.remove()` ?

Eh bien c'est la suppression de notre élément `newTask`. En précisant d'un élément qu'on souhaite le `remove()` (le _retirer_), il est retiré du document.

Un clic sur un bouton delete supprime désormais la tâche associée. Aussi simple que ça !
