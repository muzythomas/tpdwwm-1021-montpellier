# Les différences entre PHP et JS

Les langages principaux du web sont PHP et Javascript. Ces langages remplissent un rôle bien défini dans la plupart des
applications :

- PHP est un langage Back-End, qui permet d'implémenter des scripts s'exécutant "côté serveur"
- Javascript est un langage Front-End, qui s'exécute typiquement dans le navigateur, "côté client"

Ces rôles différents impliquent forcément des variations dans l'écriture du code. Certaines variations sont cependant
plus "philosophiques" ou "historiques".

Commençons par inspecter certaines de ces différences impactant les bases de notre travail :

## Différences de syntaxe

En Javascript et PHP, les différences les plus frappantes d'un point de vue syntaxiques sont les suivantes :

### 1. Déclaration de variables

En javascript :

```js
let maVariable = uneValeur;
```

En PHP :

```js
$maVariable = uneValeur; 
```

Ces différences, même si elles sont légères, impactent globalement notre façon d'écrire notre code. Le `$` étant réservé
pour les variables en PHP, il ne peut plus être utilisé d'autres façons ; par exemple l'utilisation de `String Literals`
en javascript :

```js
`J'écris une chaîne avec une ${variable} intégrée`; 
```

En PHP seules les `{}` sont nécessaires pour marquer l'intégration d'une variable (en sachant qu'elles sont plus souvent
facultatives, PHP détectant l'insertion d'une variable lors de l'utilisation de `""` sans problème).

```php
"J'écris une chaîne avec une {$variable} intégrée"; 
```

Comme on doit également utiliser `$` pour appeler nos variables à tous les coups, cela change le paysage de façon
dratstique :

```js
for (let i = 0; i < tableau.length; i++) {

}
```

en js devient en PHP :

```php
for ($i = 0; $i < count(tableau); $i++){

}
```

La différence est minime, mais il ne faut tout de même pas l'oublier !

### 2. Les `;`

En PHP, les `;` en fin de ligne sont obligatoires, pas en Javascript. Il reste tout de même intéressant de les mettre
pour des soucis de clarté.

### 3. Les tableaux

D'un point de vue syntaxe, les tableaux sont similaires, mais certaines opérations ne sont pas les même d'un langage à
l'autre.

En javascript on aura donc :

```js
let listeDeCourses = ["Pâtes", "Tomates", "Eponges", "Bières"];
```

Alors qu'en PHP :

```php
$listeDeCourses = ["Pâtes", "Tomates", "Eponges", "Bières"];
```

#### Immuabilité des données

En javascript, les tableaux sont dits "Immuables" (ou _Immutable_ en anglais). C'est-à-dire que les informations
stockées dans le tableau ne sont pas modifiables directement. On ne peut donc **pas** faire :

```js
listeDeCourses[0] = "Riz"; // IMPOSSIBLE
```

Cependant, il est possible au travers de différentes [_méthodes de
tableaux_](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array#m%C3%A9thodes_des_instances)
d'effectuer des opérations similaires. Il faut seulement être au courant que, en faisant ça, javascript se charge de _
copier l'entièreté_ du tableau d'origine, et en crée un nouveau avec les informations changées avant de le restocker
au "même endroit". Dans ces méthodes
on [retrouve notamment `Array.splice()`](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/splice)
qui permet de faire un _raccord_ sur un tableau, et donc permet également de "supprimer" un élément d'un tableau.

Alors qu'en PHP, [les tableaux](https://www.php.net/manual/fr/language.types.array.php) sont entièrement modifiables.

```php
$listeDeCourses[0] = "Riz"; //C'est possible en PHP
```

Cependant, il peut être intéressant de traiter les tableaux comme immuables en PHP pour différentes raisons, à voir au
cas par cas. Ces raisons peuvent être de performance (voir l'optimisation à l'aide d'OPCache plus tard dans votre
formation) ou rapport à la _propreté du code_ (même si cette dernière est très subjective).

#### Nombre d'éléments : _Objets Tableaux_ VS _Tableaux_

En javascript, le langage traite les Tableaux comme des sortes d'objets qui possèdent donc méthodes et propriétés. La
propriété la plus utilisée d'un tableau serait `length`, permettant de récupérer le nombre d'éléments stockés dans ledit
tableau.

En PHP, pas d'objets, seulement des tableaux. Comme aucune _propriété_ n'est disponible dans un tableau en PHP, on doit
trouver un autre moyen de récupérer le nombre d'éléments. Un des moyens est : `count()`.

Donc si en JS on peut faire :

```js
monTableau.length;
```

En PHP on doit, pour arriver à récupérer la même donnée, faire :

```php
count($monTableau);
```

Et on touche une grosse différence d'ordre quasi _philosophique_ entre Javascript et PHP :

### Objets et méthodes VS Fonctions

En Javascript, toute structure et outil du langage est sous la coupe des objets. Même si ce ne sont pas de _vrais
objets_ au sens strict du terme (Javascript appelle ça des _Prototypes_, et leur comportement diffère de celui d'objets
en Programmation Orientée Objet). Cela a un impact sur notre façon d'utiliser les différentes structures du langage. La
plupart du temps, on pourra avoir `element.property` ou `element.method()` pour invoquer certaines fonctionnalités et
valeurs intrinsèques de nos structures et composants.

Alors qu'en PHP, pour des raisons philosophiques autant que d'héritage d'anciennes versions/paradigmes, ces actions ont
tendance à se faire au travers de fonctions externes. Cependant, c'est en train de changer, et depuis PHP 7 de plus en
plus d'éléments de langage sont consignés dans des objets dont les méthodes et propriétés permettent d'utiliser des
fonctionnalités précises.

Ce qui veut dire que là où Javascript va placer une fonctionnalité comme "mettre cette chaîne de caractère en minuscule"
dans le `Prototype.String` gouvernant les chaînes de caractères du langage (`maChaine.toLowerCase()`), PHP lui utilisera
une fonction externe _à appliquer_ à la string (`mb_strtolower($maChaine)`).

Bien sûr, les documentations respectives des deux langages sont toujours facile à parcourir et consignent ce genre de
détails assez efficacement. Et dans le pire des cas, un simple `"how to [action] in [language]"` sur Google devrait
donner la réponse ;). 