<?php
//on range nos données de formulaire dans des variables plus simples à écrire
$min = $_POST["min"];
$max = $_POST["max"];
$repeats = $_POST["n"];
//on vérifie que notre répétition est faisable
if ($repeats > 0) {
    // on prépare une liste html
    echo "<ul>";
    //on répète autant de fois que nécessaire
    for ($i = 0; $i < $repeats; $i++) {
        //la génération des nombres aléatoires
        echo "<li>" . rand($min, $max) . "</li>";
    }
    //on ferme notre liste
    echo "</ul>";

} else {
    //si le nombre de répétitions est inférieur à 0 on affiche un message
    echo "Le nombre de valeurs doit être > à 0";
}

